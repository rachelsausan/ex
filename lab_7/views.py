from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    paginator = Paginator(mahasiswa_list,15)
    friend_list = Friend.objects.all()
    auth = csui_helper.instance.get_auth_param_dict()
    print(auth['access_token'])
    print(auth['client_id'])
    response = {
                "mahasiswa_list": mahasiswa_list, 
                "friend_list": friend_list,
                "auth":auth,
                }
    html = 'lab_7/lab_7.html'
    page = request.GET.get('page', 1) if int(request.GET.get('page', 1)) > 0 and int(request.GET.get('page', 1)) < 69 else 1
    
    #coba akses pagenya
    mhss = paginator.page(page)

    response['mhss']=mhss
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        alamat_mhs = request.POST['alamat_mhs']
        tgl_lahir = request.POST['tgl_lahir']
        prodi = request.POST['prodi']
        angkatan = request.POST['angkatan']
        friend = Friend(friend_name=name, npm=npm, alamat_mhs=alamat_mhs, tgl_lahir=tgl_lahir, prodi=prodi, angkatan=angkatan)
        friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data)

def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    print("coba")
    return redirect('/lab-7/get-friend-list/')

@csrf_exempt
def validate_npm(request):
    if request.method == 'POST':
        npm = request.POST.get('npm', None)
        data = {
            'is_taken': Friend.objects.filter(npm=npm).exists()
        }
        return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

def my_friend_list(request):
    friend_list = Friend.objects.all()
    data= serializers.serialize("json", friend_list)  
    return HttpResponse(data)

def view(request, friend_id):
    print("npm",friend_id)
    print(Friend.objects.filter(id=friend_id))
    friend = Friend.objects.filter(id=friend_id)[0]
    response['friend']=friend
    html = 'lab_7/view.html'
    return render(request, html, response)