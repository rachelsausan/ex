1. Membuat halaman untuk menampilkan semua mahasiswa fasilkom
	*)Terdapat list yang berisi daftar mahasiswa fasilkom, yang dipanggil dari django model.
		-Membuat model untuk Mahasiswa di models.py 
		-Di views.py,
			-mengambil list mahasiswa yang berada di api csui helper menaruhnya di mahasiswa_list.
			-kemudian pada var auth akan berisi dict. csui_helper memanggil get_auth_param_dict() yang akan membuat dict dan mengisnya dengan access_token dan client_id.
		-Memberikan list mahasiswa ke dalam response 
		Pada lab-7.html mengakses response yang telah diberikan sebelumnya dan menampilkannya ke dalam HTML. 
		-User dapat menambahkan teman melalui dua cara, yaitu dengan menginput secara manual npm dari mahasiswa tersebut dan juga melalui button yang tersedia di list mahasiswa 
		-Sementara apabila melalui manual, kita harus mengakses api csui terlebih dahulu untuk mengambil data yang dimiliki oleh mahasiswa menggunakan corsProxy, dari corsProxy itu akan mereturn data dari mahasiswa dan masuk ke dalam key success 

2. Buatlah tombol untuk dapat menambahkan list mahasiswa kedalam daftar teman (implementasikan menggunakan ajax).
	
	Pada lab_7.html terdapat Pada attribute onClick akan masuk ke dalam function addFriendFromList di function itu dia akan memvalidate npm terlebih dahulu menggunakan ajax dan apabila sukses, maka akan memanggil method addFriend 
	Data yang diberikan pada ajax di method addFriendFromList hanya berisi npm karena hanya membutuhkan data npm untuk memvalidate npm 
	Pada method addFriend, akan diberikan data ke url ajax  berupa data diri dari mahasiswa 
	Pada method addFriend di views.py akan memberikan data dalam dict menggunakan HttpResponse(data) 
	Apabila sukses, maka akan menyisipkan nama dan npm pada daftar teman 

	
	Mengimplentasikan validate_npm untuk mengecek apakah teman yang ingin dimasukkan sudah ada didalam daftar teman atau belum.

	Membuat pagination (hint: salah satu data yang didapat dari kembalian api.cs.ui.ac.id adalah next dan previous yang bisa digunakan dalam membuat pagination)


Membuat halaman untuk menampilkan daftar teman



 Terdapat list yang berisi daftar teman, data daftar teman didapat menggunakan ajax.

 Buatlah tombol untuk dapat menghapus teman dari daftar teman (implementasikan menggunakan ajax).


Pastikan kalian memiliki Code Coverage yang baik



 Jika kalian belum melakukan konfigurasi untuk menampilkan Code Coverage di Gitlab maka lihat langkah Show Code Coverage in Gitlab di README.md


 Pastikan Code Coverage kalian 100%





Additional


Membuat halaman yang menampilkan data lengkap teman 



 Halaman dibuka setiap kali user mengklik salah satu teman pada halaman yang menampilkan daftar teman

 Tambahkan google maps yang menampilkan alamat teman pada halaman informasi detail (hint: https://developers.google.com/maps/documentation/javascript/)


Berkas ".env" untuk menyimpan username dan password, dapat menyebabkan akun anda terbuka untuk orang yang memiliki 
akses ke repository bila berkas tersebut ter-push ke repository.
Hal ini sangat tidak baik dan bisa memalukan karena dapat membuka rahasia/privacy anda sendiri.



 Pastikan kerahasiaan dan privacy anda. Ubah mekanisme penyimpanan dan pengambilan bila diperlukan.