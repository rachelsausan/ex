Web Service 
Web service adalah standar yang digunakan untuk melakukan pertukaran data antar aplikasi atau sistem, karena aplikasi yang melakukan pertukaran data bisa ditulis dengan bahasa pemrograman yang berbeda atau berjalan pada platform yang berbeda. Contoh implementasi dari web service antara lain adalah SOAP dan REST. 
(sumber: https://www.codepolitan.com/mengenal-restful-web-services) 
 
Perbedaan antara SOAP dan REST: 
SOAP Itu bisa sampe memanipulasi methodnya. 
 
Web service merupakan API yang dibungkus dalam HTTP yang membuat dua mesin berbeda dapat berkomunikasi. Jika API merupakan interface untuk komunikasi antar program, web service digunakan untuk berkomunikasi antar mesin (dikenal juga dengan istilah Web Based API) melalui network. 
(sumber: https://gitlab.com/PPW-2017/ppw-lab/blob/master/lab_instruction/lab_7/README.md) 
 
Ajax 
Asynchronous JavaScript and XMLHTTP, atau disingkat AJaX, adalah suatu teknik pemrograman berbasis web untuk menciptakan aplikasi web interaktif. Tujuannya adalah untuk memindahkan sebagian besar interaksi pada komputer web surfer atau melakukan pertukaran data dengan server di belakang layar, sehingga halaman web tidak harus dibaca ulang secara keseluruhan setiap kali seorang pengguna melakukan perubahan. 
 
<script> 
$(document).ready(function(){ 
    $("#buttonPanggilAjax").click(function(){ 
        $.ajax({ 
           url: "example.com", // url yang akan dipanggil 
           Data: { data1:”contoh1” , data2:”contoh2 } // data yang akan dikirim ke example.com 
           dataType: “json” // atau apapun tipe data yang diharapkan didapat dari example.com 
           success: function(result){ 
                 // disini akan berisi code yang akan dieksekusi jika example.com berhasil dipanggil 
                 // result merupakan data yang diperoleh dari example.com 
            }, 
           Error: function(xhr, status, error) { 
                // disini akan berisi code yang akan dieksekusi jika example.com gagal dipanggil 
            } 
       }); 
    }); 
}); 
</script> 
 
 
Untuk menjaga CSRFToken agar tidak terkirim ke domain lain. 
beforeSend: function(xhr) { 
xhr.setRequestHeader("X-CSRFToken", "{{ csrf_token }}"); 
}, 
 
 
Checklist 1. Terdapat list yang berisi daftar mahasiswa fasilkom, yang dipanggil dari django model. 
Membuat model untuk Mahasiswa di models.py 
Mengambil list mahasiswa yang berada di api csui helper pada method index di views.py 
Memberikan list mahasiswa ke dalam response 
Pada lab-7.html mengakses response yang telah diberikan sebelumnya dan menampilkannya ke dalam HTML. 
User dapat menambahkan teman melalui dua cara, yaitu dengan menginput secara manual npm dari mahasiswa tersebut dan juga melalui button yang tersedia di list mahasiswa 
Sementara apabila melalui manual, kita harus mengakses api csui terlebih dahulu untuk mengambil data yang dimiliki oleh mahasiswa menggunakan corsProxy, dari corsProxy itu akan mereturn data dari mahasiswa dan masuk ke dalam key success 
 
Checklist 2.  Buatlah tombol untuk dapat menambahkan list mahasiswa kedalam daftar teman (implementasikan menggunakan ajax). 
Pada attribute onClick akan masuk ke dalam function addFriendFromList 
di function itu dia akan memvalidate npm terlebih dahulu menggunakan ajax dan apabila sukses, maka akan memanggil method addFriend 
Data yang diberikan pada ajax di method addFriendFromList hanya berisi npm karena hanya membutuhkan data npm untuk memvalidate npm 
Pada method addFriend, akan diberikan data ke url ajax  berupa data diri dari mahasiswa 
Pada method addFriend di views.py akan memberikan data dalam dict menggunakan HttpResponse(data) 
Apabila sukses, maka akan menyisipkan nama dan npm pada daftar teman 
 
 
 
Checklist 3. Mengimplentasikan validate_npm untuk mengecek apakah teman yang ingin dimasukkan sudah ada didalam daftar teman atau belum. 
Menggunakan filter dengan npm dan pakai method exists 
 
Checklist 4. Membuat pagination (hint: salah satu data yang didapat dari kembalian api.cs.ui.ac.id adalah next dan previous yang bisa digunakan dalam membuat pagination) 
Import 'from django.core.paginator import Paginator' 
Buat object paginator dengan parameter pertama itu list dan parameter kedua itu banyaknya list dalam satu page 
Membuat inisiasi page dengan: 
request.GET.get('page', 1) if int(request.GET.get('page', 1)) > 0 and int(request.GET.get('page', 1)) < 69 else 1 
Menyimpan method paginator.page(page) kedalam suatu variable yang akan mengembalikan halaman hasil paginasi, dan ini yang akan kita kembalikan ke template. 
Di template, Membuat class pagination setelah membuat list di template, seperti ini: 
<div class="pagination" style="align-self:center"> 
<span class="step-links" > 
{% if mhss.has_previous %} 
<a type="button" class="btn btn-primary btn-sm" href="?page={{ mhss.previous_page_number }}">previous</a> 
{% endif %} 
<span class="current"> 
Page {{ mhss.number }} of {{ mhss.paginator.num_pages }} 
</span> 
{% if mhss.has_next %} 
<a type="button" class="btn btn-primary btn-sm" href="?page={{ mhss.next_page_number }}">next</a> 
{% endif %} 
</span> 
</div> 
 
Additional 1. Membuat halaman yang menampilkan data lengkap teman  
Halaman dibuka setiap kali user mengklik salah satu teman pada halaman yang menampilkan daftar teman 
 
Tambahkan google maps yang menampilkan alamat teman pada halaman informasi detail (hint: https://developers.google.com/maps/documentation/javascript/) 
 
 
Additional Notes 
$.param(x) untuk membuat dari json ke bentuk string yang pakai & gitu2 
Key data dalam ajax itu adalah data yang akan dikirimkan ke url 
Data dalam parameter di function success merupakan data yang dikembalikan dari url yang dipanggil. 
Kalau mau memberikan dict pakai HttpResponse(dict) 
Kalau mau memberikan json pakai JsonResponse(Json) 
Serializers.serialize(toType,obj) untuk mengubah dari obj ke tipe toType 