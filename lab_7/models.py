# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Friend(models.Model):
    friend_name = models.CharField(max_length=400)
    npm = models.CharField(max_length=250, unique=True)
    alamat_mhs = models.CharField(max_length=400, default="-")
    tgl_lahir = models.CharField(max_length=100, default="-")
    prodi = models.CharField(max_length=100, default="-")
    angkatan = models.CharField(max_length=100, default="-")
    added_at = models.DateField(auto_now_add=True)
   
